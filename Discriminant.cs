﻿namespace DiscriminantNameSpace
{
    public class Discriminant
    {
        public double[] Solve(double a, double b , double c, double epsilon = 10e-15)
        {
            if (double.IsNaN(a) || double.IsNaN(b) || double.IsNaN(c))
                throw new ArgumentException("Коэффициенты a, b и c должны быть числами.");

            if (double.IsInfinity(a) || double.IsInfinity(c) || double.IsInfinity(c))
                throw new ArgumentException("Коэффициенты a, b и c не могут быть бесконечными.");

            if (Math.Abs(a) <= epsilon)
                throw new ArgumentException("a не равно 0");

            var D = b * b - 4 * a * c;

            if (D < -epsilon)
                return new double[] { }; //Корней нет

            if (Math.Abs(D) <= epsilon) 
                return new double[] { -b/(2 * a) }; //Один корень 

            return new double[] {(-b + Math.Sqrt(D))/(2 * a), (-b - Math.Sqrt(D))/(2 * a) }; // Два корня
        }
    }
}
